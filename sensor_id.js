const fs = require('fs')
const ini = require('ini')
const mongodb = require('mongodb')
const express = require('express')
const app = express()
const port = 80

app.use(express.json())

var config = ini.parse(fs.readFileSync(__dirname + '/config.ini', 'utf-8'))

const MongoClient = require('mongodb').MongoClient;

async function cleansing() {
	var client = await MongoClient.connect(config.mongodb.url, { useNewUrlParser: true, useUnifiedTopology: true}).catch(err => { console.log(err); });

    if (!client) {
        return;
    }

 	try {
        const db = client.db("tgr05");
        let collection = db.collection('data_warehouse');
        let query = {sensor_id: {$in:['44','45','46','47']}}
        await collection.find(query).sort({ts: 1}).limit(50).toArray(function(err, result) {
            if (err) throw err;
            
            var da = result


            // console.log(element);
            // console.log(result)

            // put your code here
            app.get('/', function (req, res) {
                res.send(da);
            })

            // app.get('/:ts', function (req, res) {
            //     const { ts } = req.params
            //     const result = da.

            // })

            // app.get('/:sensor_type', (req, res) => {
            //     var sen = da[req.params.z]
            //     console.log(sen)
            // })
            
    	    
        });
    } catch (err) {
        console.log(err);
    } finally {
        client.close();
    }
}

setInterval(cleansing, 1000);
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
