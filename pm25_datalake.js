var mqtt = require('mqtt')
var client = mqtt.connect('mqtt://202.139.192.75')

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost/tgr2020"

client.on('connect', function() {
    console.log("MQTT connection")
    client.subscribe('tgr2020/pm25/data/99')
})

client.on('message', function(topic, message) {
    // console.log("data" + topic.toString() + "=>" + message.toString())

    MongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function(err, db) {
        if (err) throw err;
        var dbo = db.db("tgr05");
        var doc = JSON.parse(message.toString());

        console.log(doc)
        dbo.collection("pm25_data_lake").insertOne(doc, function(err, res) {
            if (err) throw err;
        console.log("1 document inserted");
        db.close();
        });
    })
})